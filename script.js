import Deck from "./deck.js"
//import Chip from "./chip.js"
const deck = new Deck();
//const chip = new Chip();
//shuffle all cards
deck.shuffle();

const MAX_VALUE = 21;
const COMP_MIN_VALUE_REQUIRE = 17;
const ACE_MAX = 11;
const WIN = "bust";
const TIE = "push";
const LOSE = "lose";
const BLACK_JACK = "black jack";
//value of all chips
const CHIP_ONE = 1;
const CHIP_FIVE = 5;
const CHIP_TWENTY_FIVE = 25;
const CHIP_ONE_HUNDRED = 100;
const CHIP_FIVE_HUNDRED = 500;
let betValue = 0;
let totalChip = 500;
//count how many times player choose split
let splittedCount = 0;
//value of chips
const chipOne = document.getElementById('chip-1');
const chipFive = document.getElementById('chip-5');
const chipTwentyFive = document.getElementById('chip-25');
const chipOneHundred = document.getElementById('chip-100');

const betCircle = document.querySelector(".bet-circle");
const initialChip = document.querySelector(".total-chip");
//because computer second card is face down, so I assign second card is slot 1
const computerFirstCard = document.querySelector(".computer-card-slot-2");
const computerSecondCard = document.querySelector(".computer-card-slot-1");
const playerFirstCard = document.querySelector(".player-card-slot-1");
const playerSecondCard = document.querySelector(".player-card-slot-2");

const splitButton = document.getElementById('btn-split');
const surrenderButton = document.getElementById('btn-surrender');
const hitButton = document.getElementById('btn-hit');
const standButton = document.getElementById('btn-stand');
const doubleButton = document.getElementById('btn-double');
const betButton = document.getElementById('btn-bet');
const x2Button = document.getElementById('btn-x2');
const maxBetButton = document.getElementById('btn-max-bet');
//enable/disable function check
let enableFunction = true;
//value of 4 initial cards
const compFirstValue = deck.cards[1].getValueForBlackJack();
const compSecondValue = deck.cards[3].getValueForBlackJack();
const playerFirstValue = deck.cards[0].getValueForBlackJack();
const playerSecondValue = deck.cards[2].getValueForBlackJack();
//total values
let computerValue = compFirstValue + compSecondValue;
//create an array to hold player total card value
//help us easy to compare with dealer if player chooses split later
let playerValue = [];
playerValue[splittedCount] = playerFirstValue + playerSecondValue;

//text box result
const boxResult = document.querySelector(".box-result");

//start game
startGame();
//assign initial total chip
showTheRestOfChip();

function startGame() {
    //disable some functions
    disableButton(surrenderButton);
    disableButton(hitButton);
    disableButton(standButton);
    disableButton(doubleButton);
    disableButton(betButton);
    disableButton(x2Button);

    betButton.addEventListener('click', function(){
        betFunction();
    });
}

function betFunction() {
    //hide the second computer card
    backOfCard();
    //deal cards, player first
    computerFirstCard.appendChild(deck.cards[1].getHTML());
    computerSecondCard.appendChild(deck.cards[3].getHTML());
    playerFirstCard.appendChild(deck.cards[0].getHTML());
    playerSecondCard.appendChild(deck.cards[2].getHTML());

    //enable buttons
    //enableButton(surrenderButton);
    enableButton(hitButton);
    enableButton(standButton);
    //you can't double if you bet max
    if (betValue > totalChip) {
        disableButton(doubleButton);
    }else {
        enableButton(doubleButton);
    }
    //disable bet - you can't bet after saw the cards
    disableButton(betButton);
    disableButton(x2Button);
    disableButton(maxBetButton);
    enableFunction = false;
    chipOne.style.cursor = 'not-allowed';
    chipFive.style.cursor = 'not-allowed';
    chipTwentyFive.style.cursor = 'not-allowed';
    chipOneHundred.style.cursor = 'not-allowed';
    //initial check after deal the cards
    checkBlackjack();
    //check split
    checkSplit(deck.cards[0].getValue(), deck.cards[2].getValue());
}

//create back of card picture
function backOfCard() {
    const cardBack = document.createElement("div");
    cardBack.className = "card-back";
    const back = document.querySelector(".back");
    back.appendChild(cardBack);
}

//show the rest of the chip
function showTheRestOfChip() {
    initialChip.innerText = `${totalChip}`;
}

//reload page without change data
function reload() {
    location.reload();
    // //remove the box result
    // boxResult.remove();
    // betCircle.style.backgroundColor = "darkgreen";
    // //clear the bet circle place
    // betValue = 0;
    // betCircle.innerText = "";

    // startGame();
}

//check blackjack
function checkBlackjack() {
    //check if player blackjack
    if (playerValue[splittedCount] === MAX_VALUE) {
        if (computerValue === MAX_VALUE) {
            return showResult(TIE);
        }
        return showResult(BLACK_JACK);; 
    }
    //when computer card up is A, player can take insurance
    if (compFirstValue === ACE_MAX) {        
        console.log('insurance?')
        //enable surrender function
        enableButton(surrenderButton);
        surrenderButton.addEventListener('click', function(){
            surrenderFunction();
        });
        
    }
    //when computer card up is 10, J, Q, or K
    //check if computer gets blackjack, and if player don't -> player lose
    else if (compFirstValue === 10) {
        if ((compSecondValue === ACE_MAX) 
        && (playerValue[splittedCount] !== MAX_VALUE)) {
            //flip a face down card 
            flipCardBack();
            return showResult(LOSE);
        }
    }
}

//check how many Ace in first 2 initial cards
function checkAce(e) {
    if (e === "player") {
        if ((playerFirstValue === ACE_MAX) && (playerSecondValue === ACE_MAX)) {
            return 2;
        }
        if ((playerFirstValue === ACE_MAX) || (playerSecondValue === ACE_MAX)) {
            return 1;
        } 
    }
    else {
        if ((compFirstValue === ACE_MAX) && (compSecondValue === ACE_MAX)) {
            return 2;
        }
        if ((compFirstValue === ACE_MAX) || (compSecondValue === ACE_MAX)) {
            return 1;
        }
    }
    return 0;
}

function checkSplit(firstCard, secondCard) {
    //display split button with disable 
    splitButton.style.display = 'block';
    disableButton(splitButton);
    //if 2 cards have same value -> enable split button
    if (firstCard !== secondCard) {
        enableButton(splitButton);
    }
}

//count and check number of cards
//const player = "player";
let nextCardInDeck = 4;
let countPlayerNextCard = 1;
let countCompNextCard = 1;

hitButton.addEventListener('click', function(){
    hitFunction("player");
    //console.log(playerValue);
    if (playerValue[splittedCount] > MAX_VALUE) {
        return showResult(LOSE);;
    }
});

//normal hit function
function hitFunction(e) {
    
    //player can't double or surrender after hit
    disableButton(doubleButton);
    disableButton(surrenderButton);

    const position = `.${e}`;
    const role = document.querySelector(position);
    const className = role.className;
    getNextCardStyle(role, className);
    
    const next = deck.cards[nextCardInDeck];   //next card object
    if (e === "player") {
        const hitNextCard = `.${e} .next-card-${countPlayerNextCard}`;
        const nextCard = document.querySelector(hitNextCard);
        nextCard.appendChild(next.getHTML());
    } else {
        const hitNextCard = `.${e} .next-card-${countCompNextCard}`;
        const nextCard = document.querySelector(hitNextCard);
        nextCard.appendChild(next.getHTML());
    }
    //get the next card value
    if (splittedCount >= 1) {
        return hitFunctionAfterSplit(e, next);
    } else {
        getNextCardValue(e, next);
    }
}

//hit function after player split cards
function hitFunctionAfterSplit(e, nextCard) {
    //console.log(deck.cards[0].getValue());
    //console.log(deck.cards[nextCardInDeck].getValue());
    //player can choose double after split
    enableButton(doubleButton);
    //check again if player is able to split
    checkSplit(deck.cards[0].getValue(), deck.cards[nextCardInDeck].getValue());

    if (e === "player") {
        //special case
        if (countAceInPlayer === 2) {
            playerValue[0] = playerFirstValue + deck.cards[nextCardInDeck].getValueForBlackJack();
            nextCardInDeck += 1;
            playerValue[1] = playerSecondValue + deck.cards[nextCardInDeck].getValueForBlackJack();
            splittedCount = 0;
        }
        //count the total value and check if next card is Ace
        //=> determine Ace is 1 or 11
        if ((playerValue[splittedCount] >= 11) && (nextCard.getValue() === "A")) {
            playerValue[splittedCount] += nextCard.getAceEqualOne();    
        } 
        else {
            playerValue[splittedCount] += nextCard.getValueForBlackJack();
        }
        //last check if Ace exist, and if value > 21
        //so we should make Ace = 1 by minus 10
        if (countAceInPlayer === 1) {
            if ((playerValue[splittedCount] > MAX_VALUE)) {
                playerValue[splittedCount] -= 10;
                //set countAce = -1 to not minus 10 again
                countAceInPlayer = -1;
            }
        }
        countPlayerNextCard += 1;
        padLeftForPlayer += 2;
    //do same thing with computer
    } else {
        if (countAceInComp === 2) {
            if (nextCard.getValueForBlackJack() === 10) {
                computerValue = 2; 
            } else {
                computerValue = 12; 
            }
            countAceInComp = -1;
        }
        if ((computerValue >= 11) && (nextCard.getValue() === "A")) {
            computerValue += nextCard.getAceEqualOne();    
        } else {
            computerValue += nextCard.getValueForBlackJack();
        }
        if (countAceInComp === 1) {
            if ((computerValue > MAX_VALUE)) {
                computerValue -= 10;
                countAceInComp = -1;
            }
        }
        countCompNextCard += 1;
        padLeftForComp += 2;
    }
    nextCardInDeck += 1;
}

standButton.addEventListener('click', standFunction);

console.log(computerValue);
function standFunction() {
    //flip the face down card
    flipCardBack();
    if (splittedCount === 0) {
        //if computer values < 17, auto hit
        while (computerValue < COMP_MIN_VALUE_REQUIRE) {
            hitFunction("computer");
            //console.log(computerValue);
            //if computer value > 21, computer lose
            if (computerValue > MAX_VALUE) {
                return showResult(WIN);
            }
        }
        //if  17 <= computer values <= 21, checkResult
        //if (computerValue >= COMP_MIN_VALUE_REQUIRE) {
        if (computerValue === playerValue[splittedCount]) {
            return showResult(TIE);
        } else if (computerValue < playerValue[splittedCount]) {
            return showResult(WIN);
        } else {
            return showResult(LOSE);
        }
    }
    else if (splittedCount >= 1) {
        //splittedCount -= 1; //check split again
        padLeftForPlayer = 9; //adjust position of the next card
        hitFunction("player");

        //if computer values < 17, auto hit
        while (computerValue < COMP_MIN_VALUE_REQUIRE) {
            hitFunction("computer");
            //console.log(computerValue);
            //if computer value > 21, computer lose
            if (computerValue > MAX_VALUE) {
                return showResult(WIN);
            }
        }
        //if  17 <= computer values <= 21, checkResult
        if (computerValue === playerValue[splittedCount]) {
            return showResult(TIE);
        } else if (computerValue < playerValue[splittedCount]) {
            return showResult(WIN);
        } else {
            return showResult(LOSE);
        }
    }
}

doubleButton.addEventListener('click', doubleFunction);

//double bet function
function doubleFunction() {
    //double bet then hit 1 card
    x2BetFunction();
    //update total bet and chip
    totalChip -= betValue;
    betValue = betValue * 2;
    hitFunction("player");
    //then stand and compare result
    if (playerValue[splittedCount] > MAX_VALUE) {
        flipCardBack();
        return showResult(LOSE);
    }
    standFunction();
}


splitButton.addEventListener('click', function() {
    padLeftForPlayer = padLeftForPlayer + 7 - 2; //7 & 2 are padding left
    splittedCount += 1; //count split times
    playerValue[0] = playerFirstValue;
    playerValue[splittedCount] = playerSecondValue;
    splitFunction();
});

function splitFunction() {
    //move first card to the left
    playerFirstCard.style.marginLeft = '-7rem';
    //move second card to the right
    playerSecondCard.style.paddingLeft = '7rem';
    //special case - if player has 2 Aces
    //only hit 1 card for each group
    if (checkAce("player") === 2) {
        hitFunction("player");
        standFunction();
        hitFunction("player");
        standFunction();
    } else {
        //hit the next card
        hitFunction("player");
    }
}

//next card style
let padLeftForPlayer = 4;
let padLeftForComp = 4;
//after split
let marLeft = -5;

function getNextCardStyle(role, name) {
    const nextCard = document.createElement("div");
    if (name === "player") {
        nextCard.className = `next-card-${countPlayerNextCard}`;
        if (splittedCount === 0) {
            nextCard.style.paddingLeft = `${padLeftForPlayer}rem`;
        }
        if (splittedCount >= 1) {
            nextCard.style.marginLeft = `${marLeft}rem`;
            marLeft += 2;
        }
    } if (name === "computer") {
        nextCard.className = `next-card-${countCompNextCard}`;
        nextCard.style.paddingLeft = `${padLeftForComp}rem`;
    }
    nextCard.style.height = '8rem';
    nextCard.style.position = 'absolute';
    role.appendChild(nextCard);
}

//check how many Ace card to determine value
let countAceInPlayer = checkAce("player");
let countAceInComp = checkAce("computer");

function getNextCardValue(e, nextCard) {
    if (e === "player") {
        //special case
        if (countAceInPlayer === 2) {
            if (nextCard.getValueForBlackJack() === 10) {
                playerValue[splittedCount] = 2; //each Ace = 1
            } else {
                playerValue[splittedCount] = 12; //1 Ace = 1, another = 11
            }
            //make countAce = -1 to not repeat assign playerValue again
            countAceInPlayer = -1;
        }
        //count the total value and check if next card is Ace
        //=> determine Ace is 1 or 11
        if ((playerValue[splittedCount] >= 11) && (nextCard.getValue() === "A")) {
            playerValue[splittedCount] += nextCard.getAceEqualOne();    
        } 
        else {
            playerValue[splittedCount] += nextCard.getValueForBlackJack();
        }
        //last check if Ace exist, and if value > 21
        //so we should make Ace = 1 by minus 10
        if (countAceInPlayer === 1) {
            if ((playerValue[splittedCount] > MAX_VALUE)) {
                playerValue[splittedCount] -= 10;
                //set countAce = -1 to not minus 10 again
                countAceInPlayer = -1;
            }
        }
        countPlayerNextCard += 1;
        padLeftForPlayer += 2;
    //do same thing with computer
    } else {
        if (countAceInComp === 2) {
            if (nextCard.getValueForBlackJack() === 10) {
                computerValue = 2; 
            } else {
                computerValue = 12; 
            }
            countAceInComp = -1;
        }
        if ((computerValue >= 11) && (nextCard.getValue() === "A")) {
            computerValue += nextCard.getAceEqualOne();    
        } else {
            computerValue += nextCard.getValueForBlackJack();
        }
        if (countAceInComp === 1) {
            if ((computerValue > MAX_VALUE)) {
                computerValue -= 10;
                countAceInComp = -1;
            }
        }
        countCompNextCard += 1;
        padLeftForComp += 2;
    }
    nextCardInDeck += 1;
}

//surrender function
function surrenderFunction() {
    //you lost a half if you surrender
    betValue = betValue / 2;
    totalChip += betValue;
    showResult(LOSE);
}

function flipCardBack() {
    const flipCard = document.querySelector(".card-back");
    //computerSecondCard.style.position = 'relative';
    flipCard.style.zIndex = '-1';
}

//disable button function
function disableButton(e) {
    e.disabled = true;
    e.style.opacity = '20%';
    e.style.cursor = 'not-allowed';
    e.style.color = 'black';
}

//enable button function
function enableButton(e) {
    //pseudo class like :hover never refer to an element
    //=> mouseOver and mouseOut replace for btn:hover
    e.onmouseover = function() { this.style.color = 'white'; };
    e.onmouseout = function() { e.style.color = 'black' };
    e.disabled = false;
    e.style.opacity = '100%';
    e.style.cursor = 'pointer';
}

chipOne.addEventListener('click', function(){
    addChip(CHIP_ONE);
});
chipFive.addEventListener('click', function(){
    addChip(CHIP_FIVE);
});
chipTwentyFive.addEventListener('click', function(){
    addChip(CHIP_TWENTY_FIVE);
});
chipOneHundred.addEventListener('click', function(){
    addChip(CHIP_ONE_HUNDRED);
});

function addChip(e) {
    //you can only use hit, stand,.. functions after you place the bet
    if (enableFunction == true) {
        enableButton(betButton);
        enableButton(x2Button);
        //you can't bet more than you have
        if (totalChip > 0) {
            betValue += e;
            totalChip -= e;
        } else {
            window.alert("You don't have enough chip!")
        }
        if (betValue < CHIP_FIVE) {
            betCircle.style.backgroundColor = "blue";
        } else if ((betValue >= CHIP_FIVE) && (betValue < CHIP_TWENTY_FIVE)) {
            betCircle.style.backgroundColor = "red";
        } else if ((betValue >= CHIP_TWENTY_FIVE) && (betValue < CHIP_ONE_HUNDRED)) {
            betCircle.style.backgroundColor = "green";
        } else if ((betValue >= CHIP_ONE_HUNDRED) && (betValue < CHIP_FIVE_HUNDRED)) {
            betCircle.style.backgroundColor = "black";
        } else {
            betCircle.style.backgroundColor = "orange";
        }
        betCircle.innerText = `${betValue}`;
        showTheRestOfChip();
    } else {
        return;
    }
}

function playAgain() {
    const btnPlayAgain = document.createElement("button");
    btnPlayAgain.className = "btn play-again";
    btnPlayAgain.innerText = "Play Again";
    btnPlayAgain.addEventListener('click', function() {
        btnPlayAgain.remove();
        reload();
    });
    const playAgain = document.querySelector(".play-again");
    playAgain.appendChild(btnPlayAgain);
}

//style of box result
function boxResultStyle() {
    boxResult.style.position = 'absolute';
    boxResult.style.width = '400px';
    boxResult.style.height = '200px';
    boxResult.style.backgroundColor = 'white';
    boxResult.style.borderRadius = '.5rem';
    boxResult.style.marginLeft = '4rem';
    boxResult.style.opacity = '85%';
    boxResult.style.boxShadow = '0px 0px 20px 20px gray';
    boxResult.classList.add("text-result");
}

//show result
function showResult(e) {
    boxResultStyle();
    playAgain();
    console.log(e);
    console.log(computerValue);
    console.log(playerValue[splittedCount]);

    if(e === LOSE) {
        boxResult.innerText = `You Lose -${betValue}`;
        //totalChip -= betValue;    
    } else if(e === TIE) {
        boxResult.innerText = "Push(Tie)!";
        totalChip += betValue;
    } else if (e === BLACK_JACK) {
        //return one and a half for blackjack
        totalChip += betValue;
        betValue += betValue/2;
        totalChip += betValue;
        boxResult.innerText = `BlackJack! +${betValue}`;
    } else {
        betValue = betValue * 2;
        boxResult.innerText = `You Win +${betValue}`;
        totalChip += betValue;
    }
    showTheRestOfChip();
}

x2Button.addEventListener('click', x2BetFunction);

function x2BetFunction() {
    //you can't double if you have less chip than you bet
    if (totalChip >= betValue) {
        addChip(betValue);
    } else {
        window.alert("You don't have enough chip!");
        disableButton(doubleButton);
        return;
    }
}

maxBetButton.addEventListener('click', maxBetFunction);
//max bet - all in what you have
function maxBetFunction() {
    addChip(totalChip);
}
const helpButton = document.getElementById('btn-help')
.addEventListener('click', function(){
    window.open("https://bicyclecards.com/how-to-play/blackjack/", "popUpWindow", "_blank");
});
